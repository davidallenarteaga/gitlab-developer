# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '7.17.1'
  end
end
