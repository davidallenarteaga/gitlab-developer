# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class ObjectStorage < Default
          TYPES = %w[artifacts external_diffs lfs uploads packages dependency_proxy].freeze

          def configuration
            TYPES.each_with_object(+'') do |object_type, omnibus|
              omnibus << <<~OMNIBUS
                gitlab_rails['#{object_type}_enabled'] = true
                gitlab_rails['#{object_type}_storage_path'] = '/var/opt/gitlab/gitlab-rails/shared/#{object_type}'
                gitlab_rails['#{object_type}_object_store_enabled'] = true
                gitlab_rails['#{object_type}_object_store_remote_directory'] = '#{object_type}-bucket'
                gitlab_rails['#{object_type}_object_store_background_upload'] = false
                gitlab_rails['#{object_type}_object_store_direct_upload'] = true
                gitlab_rails['#{object_type}_object_store_proxy_download'] = true
                gitlab_rails['#{object_type}_object_store_connection'] = #{minio.to_config}
              OMNIBUS
            end
          end

          def prepare
            minio.network = 'test'

            TYPES.each do |bucket_name|
              minio.add_bucket("#{bucket_name}-bucket")
            end

            minio
          end

          def exec_commands
            QA::Scenario::CLICommands.git_lfs_install_commands
          end

          private

          def minio
            @minio ||= Component::Minio.new
          end
        end
      end
    end
  end
end
