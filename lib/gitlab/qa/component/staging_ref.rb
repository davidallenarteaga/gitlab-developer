module Gitlab
  module QA
    module Component
      class StagingRef < Staging
        ADDRESS = 'https://staging-ref.gitlab.com'.freeze
      end
    end
  end
end
