##############
# Conditions #
##############
.if-default-refs-or-ce: &if-default-refs-or-ce
  if: '$RELEASE == null || $RELEASE =~ /gitlab-ce/ || $CI_MERGE_REQUEST_ID || $CI_COMMIT_REF_NAME == "master"'

.if-default-refs-or-ee: &if-default-refs-or-ee
  if: '$RELEASE == null || $RELEASE =~ /gitlab-ee/ || $CI_MERGE_REQUEST_ID || $CI_COMMIT_REF_NAME == "master"'

.if-not-gitlab-qa-release: &if-not-gitlab-qa-release
  if: '$CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_PATH == "gitlab-org/gitlab-qa" && $RELEASE == null'

.if-staging-and-not-release: &if-staging-and-not-release
  if: '$RELEASE == null && $CI_JOB_NAME =~ /staging/'

.if-quarantine-or-custom-and-not-release: &if-quarantine-or-custom-and-not-release
  if: '$RELEASE == null && $CI_JOB_NAME =~ /quarantine|custom/'

.if-quarantine-or-custom-and-ee-release: &if-quarantine-or-custom-and-ee-release
  if: '$RELEASE =~ /gitlab-ee/ && $CI_JOB_NAME =~ /quarantine|custom/'

.if-quarantine-or-custom-and-ce-release: &if-quarantine-or-custom-and-ce-release
  if: '$RELEASE =~ /gitlab-ce/ && $CI_JOB_NAME =~ /quarantine|custom/'

.if-quarantine-or-custom-and-merge-request: &if-quarantine-or-custom-and-merge-request
  if: '$CI_MERGE_REQUEST_ID && $CI_JOB_NAME =~ /quarantine|custom/'

.if-tag-or-ee-release: &if-tag-or-ee-release
  if: '$CI_COMMIT_TAG || $RELEASE =~ /gitlab-ee/'

.if-tag-or-ce-release: &if-tag-or-ce-release
  if: '$CI_COMMIT_TAG || $RELEASE =~ /gitlab-ce/'

.if-tag-or-release: &if-tag-or-release
  if: '$CI_COMMIT_TAG || $RELEASE'

.if-set-feature-flag: &if-set-feature-flag
  if: '$GITLAB_QA_OPTIONS =~ /--set-feature-flags/'

.if-qa-tests-specified: &if-qa-tests-specified
  if: '$QA_TESTS != null && $QA_TESTS != ""'

.if-qa-tests-not-specified: &if-qa-tests-not-specified
  if: '$QA_TESTS == null || $QA_TESTS == ""'

#########
# Rules #
#########

.rules:ce-qa:
  rules:
    - <<: *if-not-gitlab-qa-release
      changes: ["lib/**/version.rb"]
      when: never
    - <<: *if-tag-or-ee-release
      when: never
    - <<: *if-quarantine-or-custom-and-not-release
      when: manual
    - <<: *if-quarantine-or-custom-and-ce-release
      when: manual
    - <<: *if-quarantine-or-custom-and-merge-request
      when: manual
    - <<: *if-default-refs-or-ce

.rules:ee-qa:
  rules:
    - <<: *if-not-gitlab-qa-release
      changes: ["lib/**/version.rb"]
      when: never
    - <<: *if-tag-or-ce-release
      when: never
    - <<: *if-quarantine-or-custom-and-not-release
      when: manual
    - <<: *if-quarantine-or-custom-and-ee-release
      when: manual
    - <<: *if-quarantine-or-custom-and-merge-request
      when: manual
    - <<: *if-default-refs-or-ee

.rules:only-qa:
  rules:
    - <<: *if-not-gitlab-qa-release
      changes: ["lib/**/version.rb"]
      when: never
    - <<: *if-tag-or-release
      when: never
    - <<: *if-staging-and-not-release
      when: manual

.rules:ce-never-when-triggered-by-feature-flag-definition-change:
  rules:
    - <<: *if-set-feature-flag
      when: never
    - !reference [".rules:ce-qa", rules]

.rules:ee-never-when-triggered-by-feature-flag-definition-change:
  rules:
    - <<: *if-set-feature-flag
      when: never
    - !reference [".rules:ee-qa", rules]

.rules:only-qa-never-when-triggered-by-feature-flag-definition-change:
  rules:
    - <<: *if-set-feature-flag
      when: never
    - !reference [".rules:only-qa", rules]

# === When QA_TESTS variable has a value ===

.rules:ce-never-when-qa-tests-specified:
  rules:
    - <<: *if-qa-tests-specified
      when: never
    - !reference [".rules:ce-qa", rules]

.rules:ee-never-when-qa-tests-specified:
  rules:
    - <<: *if-qa-tests-specified
      when: never
    - !reference [".rules:ee-qa", rules]

.rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified:
  rules:
    - <<: *if-qa-tests-specified
      when: never
    - !reference [".rules:ce-never-when-triggered-by-feature-flag-definition-change", rules]

.rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified:
  rules:
    - <<: *if-qa-tests-specified
      when: never
    - !reference [".rules:ee-never-when-triggered-by-feature-flag-definition-change", rules]

.rules:only-qa-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified:
  rules:
    - <<: *if-qa-tests-specified
      when: never
    - !reference [".rules:only-qa-never-when-triggered-by-feature-flag-definition-change", rules]

# === When QA_TESTS variable does not have a value ===

.rules:ce-never-when-qa-tests-not-specified:
  rules:
    - <<: *if-qa-tests-not-specified
      when: never
    - !reference [".rules:ce-qa", rules]

.rules:ee-never-when-qa-tests-not-specified:
  rules:
    - <<: *if-qa-tests-not-specified
      when: never
    - !reference [".rules:ee-qa", rules]

.rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified:
  rules:
    - <<: *if-qa-tests-not-specified
      when: never
    - !reference [".rules:ce-never-when-triggered-by-feature-flag-definition-change", rules]

.rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified:
  rules:
    - <<: *if-qa-tests-not-specified
      when: never
    - !reference [".rules:ee-never-when-triggered-by-feature-flag-definition-change", rules]

.rules:only-qa-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified:
  rules:
    - <<: *if-qa-tests-not-specified
      when: never
    - !reference [".rules:only-qa-never-when-triggered-by-feature-flag-definition-change", rules]
