stages:
  - check
  - test
  - report
  - deploy
  - notify

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-qa-ruby-2.7
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile.lock
        - gitlab-qa.gemspec
    paths:
      - vendor/ruby
  before_script:
    - echo $GITLAB_QA_OPTIONS
    - bundle version
    - bundle config path vendor
    - bundle install --jobs=$(nproc) --retry=3 --quiet && bundle check
    - if [ -n "$TRIGGERED_USER" ] && [ -n "$TRIGGER_SOURCE" ]; then
        echo "Pipeline triggered by $TRIGGERED_USER at $TRIGGER_SOURCE";
      fi
    - export LANG=C.UTF-8

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For the default branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == "master"'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'
    # For triggers from GitLab MR pipelines (and pipelines from other projects), create a pipeline
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'
    # When using Run pipeline button in the GitLab UI, from the project’s CI/CD > Pipelines section, create a pipeline.
    - if: '$CI_PIPELINE_SOURCE == "web"'

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375
  QA_ARTIFACTS_DIR: $CI_PROJECT_DIR
  QA_CAN_TEST_GIT_PROTOCOL_V2: "true"
  QA_CAN_TEST_PRAEFECT: "false"
  QA_GENERATE_ALLURE_REPORT: "true"
  QA_TESTCASES_REPORTING_PROJECT: "gitlab-org/gitlab"
  QA_TEST_RESULTS_ISSUES_PROJECT: "gitlab-org/quality/testcases"
  QA_TESTCASE_SESSIONS_PROJECT: "gitlab-org/quality/testcase-sessions"
  # QA_DEFAULT_BRANCH is the default branch name of the instance under test.
  QA_DEFAULT_BRANCH: "master"
  # TOP_UPSTREAM_DEFAULT_BRANCH is the default branch name of the original project that triggered a pipeline in this project.
  TOP_UPSTREAM_DEFAULT_BRANCH: "master"

.check-base:
  stage: check
  script:
    - bundle exec $CI_JOB_NAME --color

rubocop:
  extends: .check-base

rspec:
  extends: .check-base

.test:
  stage: test
  timeout: 1 hour 30 minutes
  services:
    - docker:20.10.5-dind
  tags:
    - gitlab-org
    - docker
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - gitlab-qa-run-*
    reports:
      junit: gitlab-qa-run-*/**/rspec-*.xml
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS || test_run_exit_code=$?
    - bundle exec exe/gitlab-qa-report --update-screenshot-path "gitlab-qa-run-*/**/rspec-*.xml"
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - if [ "$TOP_UPSTREAM_SOURCE_REF" == $TOP_UPSTREAM_DEFAULT_BRANCH ] || [[ "$TOP_UPSTREAM_SOURCE_JOB" == https://ops.gitlab.net* ]]; then exe/gitlab-qa-report --report-results "gitlab-qa-run-*/**/rspec-*.json" --test-case-project "$QA_TESTCASES_REPORTING_PROJECT" --results-issue-project "$QA_TEST_RESULTS_ISSUES_PROJECT" || true; fi
    - exit $test_run_exit_code

# For jobs that shouldn't report results in issues, e.g., manually run custom jobs
.no-issue-report-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS

# For jobs that provide additional GITLAB_QA_OPTIONS, e.g., jobs that include --omnibus-config
.combined-gitlab-qa-options-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS_COMBINED -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS_COMBINED -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS

# The Test::Omnibus::Update scenarios require the release to be specified twice, which can't be done dynamically using the `variables` parameter
# So instead we can use this script with two release variables
.update-scenario-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=$DEFAULT_RELEASE} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=$DEFAULT_RELEASE} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS || test_run_exit_code=$?
    - bundle exec exe/gitlab-qa-report --update-screenshot-path "gitlab-qa-run-*/**/rspec-*.xml"
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - if [ "$TOP_UPSTREAM_SOURCE_REF" == $TOP_UPSTREAM_DEFAULT_BRANCH ] || [[ "$TOP_UPSTREAM_SOURCE_JOB" == https://ops.gitlab.net* ]]; then exe/gitlab-qa-report --report-results "gitlab-qa-run-*/**/rspec-*.json" --test-case-project "$QA_TESTCASES_REPORTING_PROJECT" --results-issue-project "$QA_TEST_RESULTS_ISSUES_PROJECT" || true; fi
    - exit $test_run_exit_code

.ce-variables:
  variables:
    DEFAULT_RELEASE: "CE"

.ee-variables:
  variables:
    DEFAULT_RELEASE: "EE"

.high-capacity:
  tags:
    - docker
    - 7gb
    - triggered-packages

.knapsack-variables:
  variables:
    KNAPSACK_REPORT_PATH: "knapsack/master_report.json"
    KNAPSACK_TEST_FILE_PATTERN: "qa/specs/features/**/*_spec.rb"

.rspec-report-opts:
  variables:
    FILE_SAFE_JOB_NAME: $(echo $CI_JOB_NAME | sed 's/[ /]/_/g')
    RSPEC_REPORT_OPTS: "--format QA::Support::JsonFormatter --out \"tmp/rspec-${CI_JOB_ID}.json\" --format RspecJunitFormatter --out \"tmp/rspec-${CI_JOB_ID}.xml\" --format html --out \"tmp/rspec-${FILE_SAFE_JOB_NAME}.htm\" --color --format documentation"

.quarantine:
  allow_failure: true
  variables:
    QA_RSPEC_TAGS: "--tag quarantine"

# Do not generate allure report since it always adds a failed test to the report. The job runs a test where 1 example would always fail and assert exit code via custom script bin/expect_exit_code_and_text
ce:sanity-framework:
  variables:
    QA_GENERATE_ALLURE_REPORT: "false"
    QA_EXPORT_TEST_METRICS: "false"
  script:
    - ./bin/expect_exit_code_and_text "bundle exec exe/gitlab-qa Test::Instance::Image ${RELEASE:=CE} -- --tag framework" 1 "2 examples, 1 failure"
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables

ee:sanity-framework:
  variables:
    QA_GENERATE_ALLURE_REPORT: "false"
    QA_EXPORT_TEST_METRICS: "false"
  script:
    - ./bin/expect_exit_code_and_text "bundle exec exe/gitlab-qa Test::Instance::Image ${RELEASE:=EE} -- --tag framework" 1 "2 examples, 1 failure"
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables

ce:custom-parallel:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .rspec-report-opts
    - .no-issue-report-script
    - .ce-variables
  allow_failure: true
  parallel: 10

ee:custom-parallel:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .no-issue-report-script
  allow_failure: true
  parallel: 10

ce:instance-parallel:
  extends:
    - .rules:ce-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ce:instance:
  extends:
    - .rules:ce-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts

ce:instance-quarantine:
  extends:
    - .rules:ce-qa
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ee:instance-parallel:
  extends:
    - .rules:ee-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5

ee:instance:
  extends:
    - .rules:ee-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts

ee:instance-quarantine:
  extends:
    - .rules:ee-qa
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ce:relative_url-parallel:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"

ce:relative_url:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"

ce:relative_url-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ee:relative_url-parallel:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"

ee:relative_url:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"

ee:relative_url-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RelativeUrl"
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ce:repository_storage:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RepositoryStorage"

ce:repository_storage-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RepositoryStorage"

ee:repository_storage:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RepositoryStorage"

ee:repository_storage-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Instance::RepositoryStorage"

# The Test::Omnibus::Image scenarios don't run the E2E tests so they don't need to report test results
ce:image:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Image ${RELEASE:=CE}
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .ce-variables

ee:image:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Image ${RELEASE:=EE}
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .ee-variables

ce:update-parallel:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
    - .knapsack-variables
    - .update-scenario-script
  parallel: 5

ce:update:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
    - .update-scenario-script

ce:update-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
    - .update-scenario-script
  variables:
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ee:update-parallel:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .knapsack-variables
    - .update-scenario-script
  parallel: 10

ee:update:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .update-scenario-script

ee:update-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

# The Test::Omnibus::Upgrade scenario isn't run on master (because it always uses the latest CE/EE image) so we don't report the test results in issues
ce:upgrade-parallel:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Upgrade CE -- $RSPEC_REPORT_OPTS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .rspec-report-opts
    - .knapsack-variables
  parallel: 5

ce:upgrade:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Upgrade CE -- $RSPEC_REPORT_OPTS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .rspec-report-opts

ce:upgrade-quarantine:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Upgrade CE -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .quarantine
    - .rspec-report-opts

ee-previous-to-ce:update:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Update EE CE -- $RSPEC_REPORT_OPTS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .rspec-report-opts

ee-previous-to-ce:update-quarantine:
  script:
    - bundle exec exe/gitlab-qa Test::Omnibus::Update EE CE -- --tag quarantine --tag ~orchestrated $RSPEC_REPORT_OPTS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .quarantine
    - .rspec-report-opts

ce:mattermost:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Mattermost"

ce:mattermost-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Mattermost"

ee:mattermost:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Mattermost"

ee:mattermost-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Mattermost"

ce:service_ping_disabled:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::ServicePingDisabled"

ee:service_ping_disabled:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::ServicePingDisabled"

# Disabling geo jobs temporarily due to https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/774
# ee:geo:
#   extends:
#     - .test
#     - .ee-variables
#     - .rules:ee-never-when-triggered-by-feature-flag-definition-change
#     - .rspec-report-opts
#   variables:
#     QA_SCENARIO: "Test::Integration::Geo"

# ee:geo-quarantine:
#   extends:
#     - .test
#     - .ee-variables
#     - .rules:ee-never-when-triggered-by-feature-flag-definition-change
#     - .quarantine
#     - .rspec-report-opts
#   variables:
#     QA_SCENARIO: "Test::Integration::Geo"

ce:ldap_no_tls:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoTLS"

ce:ldap_no_tls-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoTLS"

ee:ldap_no_tls:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoTLS"

ee:ldap_no_tls-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoTLS"

ce:ldap_tls:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPTLS"

ce:ldap_tls-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPTLS"

ee:ldap_tls:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPTLS"

ee:ldap_tls-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPTLS"

ee:ldap_no_server:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoServer"

ee:ldap_no_server-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::LDAPNoServer"

ce:instance_saml:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::InstanceSAML"

ce:instance_saml-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::InstanceSAML"

ee:instance_saml:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::InstanceSAML"

ee:instance_saml-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::InstanceSAML"

ee:group_saml:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GroupSAML"

ee:group_saml-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GroupSAML"

ce:object_storage:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage"

ce:object_storage-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage"

ee:object_storage_aws:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage_aws"
    QA_RSPEC_TAGS: "--tag object_storage"

ee:object_storage_aws-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage_aws"
    QA_RSPEC_TAGS: "--tag quarantine --tag object_storage"

ee:object_storage_gcs:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    GITLAB_QA_OPTS: "--omnibus-config object_storage_gcs"
    QA_RSPEC_TAGS: "--tag object_storage"

ee:object_storage_gcs-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    GITLAB_QA_OPTS: "--omnibus-config object_storage_gcs"
    QA_RSPEC_TAGS: "--tag quarantine --tag object_storage"

ee:object_storage:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage"
    QA_RSPEC_TAGS: "--tag object_storage"

ee:object_storage-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config object_storage"
    QA_RSPEC_TAGS: "--tag quarantine --tag object_storage"

ee:packages:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config packages"
    QA_RSPEC_TAGS: "--tag packages"

ee:packages-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
    - .combined-gitlab-qa-options-script
  variables:
    GITLAB_QA_OPTIONS_COMBINED: "$GITLAB_QA_OPTIONS --omnibus-config packages"
    QA_RSPEC_TAGS: "--tag quarantine --tag packages"

ce:object_storage_registry_tls:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::RegistryTLS --omnibus-config registry_object_storage"

ce:object_storage_registry_tls-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::RegistryTLS --omnibus-config registry_object_storage"

ee:object_storage_registry_tls:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .ee-variables
    - .high-capacity
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::RegistryTLS --omnibus-config registry_object_storage"

ee:object_storage_registry_tls-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::RegistryTLS --omnibus-config registry_object_storage"

ce:registry:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Registry"

ce:registry-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Registry"

ee:registry:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Registry"

ee:registry-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Registry"

ce:actioncable:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Actioncable"

ce:actioncable-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Actioncable"

ee:actioncable:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Actioncable"

ee:actioncable-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Actioncable"

ee:elasticsearch:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Elasticsearch"

ee:elasticsearch-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Elasticsearch"

ce:praefect-parallel:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 5
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"

ce:praefect:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"

ce:praefect-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ee:praefect-parallel:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .knapsack-variables
    - .rspec-report-opts
  parallel: 10
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"

ee:praefect:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change-and-never-when-qa-tests-not-specified
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"

ee:praefect-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Praefect"
    QA_CAN_TEST_PRAEFECT: "true"
    QA_RSPEC_TAGS: "--tag quarantine --tag ~orchestrated"

ce:gitaly-cluster:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GitalyCluster"
    QA_LOG_PATH: "tmp/gitaly_cluster.log"

ce:gitaly-cluster-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GitalyCluster"
    QA_LOG_PATH: "tmp/gitaly_cluster.log"

ee:gitaly-cluster:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GitalyCluster"
    QA_LOG_PATH: "tmp/gitaly_cluster.log"

ee:gitaly-cluster-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::GitalyCluster"
    QA_LOG_PATH: "tmp/gitaly_cluster.log"

ce:mtls:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::MTLS"

ee:mtls:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::MTLS"

ce:smtp:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::SMTP"

ee:smtp:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::SMTP"

ce:jira:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Jira"

ce:jira-quarantine:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Jira"

ee:jira:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Jira"

ee:jira-quarantine:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .quarantine
    - .rspec-report-opts
  variables:
    QA_SCENARIO: "Test::Integration::Jira"

ce:large-setup:
  extends:
    - .rules:ce-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ce-variables
    - .rspec-report-opts
  variables:
    QA_RSPEC_TAGS: "--tag can_use_large_setup"

ee:large-setup:
  extends:
    - .rules:ee-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
    - .ee-variables
    - .rspec-report-opts
  variables:
    QA_RSPEC_TAGS: "--tag can_use_large_setup"

# This job requires the `GITLAB_QA_ACCESS_TOKEN` and `GITLAB_QA_DEV_ACCESS_TOKEN`
# variable to be passed when triggered.
staging:
  script:
    - unset EE_LICENSE
    - 'echo "Running: bundle exec exe/gitlab-qa Test::Instance::Staging ${RELEASE:=$DEFAULT_RELEASE} -- $QA_TESTS $QA_RSPEC_TAGS"'
    - bundle exec exe/gitlab-qa Test::Instance::Staging ${RELEASE:=$DEFAULT_RELEASE} -- $QA_TESTS $QA_RSPEC_TAGS
  extends:
    - .rules:only-qa-never-when-triggered-by-feature-flag-definition-change
    - .test
    - .high-capacity
  allow_failure: true

generate_test_session:
  stage: report
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_JOB && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: always
    - if: '$TOP_UPSTREAM_SOURCE_JOB =~ /\Ahttps:\/\/ops.gitlab.net\//'
      when: always
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - REPORT_ISSUE_URL
  script:
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - bundle exec exe/gitlab-qa-report --generate-test-session "gitlab-qa-run-*/**/rspec-*.json" --project "$QA_TESTCASE_SESSIONS_PROJECT"

relate_test_failures:
  stage: report
  rules:
    - if: '$DISABLE_RELATING_FAILURE_ISSUES'
      when: never
    - if: '$TOP_UPSTREAM_SOURCE_JOB && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: always
    - if: '$TOP_UPSTREAM_SOURCE_JOB =~ /\Ahttps:\/\/ops.gitlab.net\//'
      when: always
  variables:
    QA_FAILURES_REPORTING_PROJECT: "gitlab-org/gitlab"
    QA_FAILURES_MAX_DIFF_RATIO: "0.15"
    # The --dry-run can be set to modify the behavior of `exe/gitlab-qa-report --relate-failure-issue` without releasing a new gem version.
    QA_FAILURES_REPORTER_OPTIONS: ""
  script:
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - bundle exec exe/gitlab-qa-report --relate-failure-issue "gitlab-qa-run-*/**/rspec-*.json" --project "$QA_FAILURES_REPORTING_PROJECT" --max-diff-ratio "$QA_FAILURES_MAX_DIFF_RATIO" $QA_FAILURES_REPORTER_OPTIONS

.notify_upstream_commit:
  stage: notify
  image: ruby:3.0-alpine
  before_script:
    - gem install gitlab --no-document

notify_upstream_commit:success:
  extends: .notify_upstream_commit
  script:
    - bin/notify_upstream_commit success
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_PROJECT && $TOP_UPSTREAM_SOURCE_SHA'
      when: on_success

notify_upstream_commit:failure:
  extends: .notify_upstream_commit
  script:
    - bin/notify_upstream_commit failure
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_PROJECT && $TOP_UPSTREAM_SOURCE_SHA'
      when: on_failure

.notify_slack:
  image: alpine
  stage: notify
  dependencies: ['generate_test_session']
  cache: {}
  before_script:
    - apk update && apk add git curl bash

notify_slack:
  extends:
    - .notify_slack
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_JOB && $NOTIFY_CHANNEL'
      when: on_failure
    - if: '$TOP_UPSTREAM_SOURCE_JOB && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: on_failure
  script:
    - export RELEASE=${TOP_UPSTREAM_SOURCE_REF:-$RELEASE}
    - echo "NOTIFY_CHANNEL is ${NOTIFY_CHANNEL:=qa-$TOP_UPSTREAM_SOURCE_REF}"
    - echo "RELEASE is ${RELEASE}"
    - echo "CI_PIPELINE_URL is $CI_PIPELINE_URL"
    - echo "TOP_UPSTREAM_SOURCE_JOB is $TOP_UPSTREAM_SOURCE_JOB"
    - 'bin/slack $NOTIFY_CHANNEL "☠️ QA against $RELEASE failed! ☠️ See the test session report: $(cat REPORT_ISSUE_URL), and pipeline: $CI_PIPELINE_URL (triggered from $TOP_UPSTREAM_SOURCE_JOB)" ci_failing'

generate-allure-report:
  extends: .generate-allure-report-base
  stage: report
  variables:
    STORAGE_CREDENTIALS: $QA_ALLURE_REPORT_GCS_CREDENTIALS
    GITLAB_AUTH_TOKEN: $GITLAB_QA_MR_ALLURE_REPORT_TOKEN
    ALLURE_RESULTS_GLOB: gitlab-qa-run-*/**/allure-results/*
    # Override allure variables for upstream triggers
    ALLURE_PROJECT_PATH: $TOP_UPSTREAM_SOURCE_PROJECT
    ALLURE_MERGE_REQUEST_IID: $TOP_UPSTREAM_MERGE_REQUEST_IID
    ALLURE_COMMIT_SHA: $TOP_UPSTREAM_SOURCE_SHA
  # Override default before script and don't push cache
  cache:
    policy: pull
  before_script:
    - export ALLURE_JOB_NAME="${ALLURE_JOB_NAME:-package-and-qa}"
  rules:
    # Don't run report generation on release pipelines
    - if: '$CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_PATH == "gitlab-org/gitlab-qa" && $RELEASE == null'
      changes: ["lib/**/version.rb"]
      when: never
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || ($CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH)'
      variables:
        ALLURE_JOB_NAME: gitlab-qa
      when: always
    - if: '$TOP_UPSTREAM_SOURCE_PROJECT && $TOP_UPSTREAM_SOURCE_SHA'
      when: always

generate-knapsack-report:
  image:
    name: ${QA_IMAGE}
    entrypoint: [""]
  stage: report
  before_script:
    - cd /home/gitlab/qa
  script:
    - bundle exec rake "knapsack:upload[$CI_PROJECT_DIR/gitlab-qa-run-*/gitlab-*-qa-*/knapsack/*/*.json]"
  rules:
    - if: '$KNAPSACK_GENERATE_REPORT == "true"'
      when: always

include:
  - local: .gitlab/ci/*.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file: '/ci/gem-release.yml'
  - project: 'gitlab-org/quality/pipeline-common'
    file: '/ci/allure-report.yml'
